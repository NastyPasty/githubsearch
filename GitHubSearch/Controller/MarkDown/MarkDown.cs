﻿using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Threading.Tasks;

namespace GitHubSearch.MarkDown
{

    [HtmlTargetElement("markdown")]
    public class MarkdownTagHelper : TagHelper
    {
        [HtmlAttributeName("normalize-whitespace")]
        public bool NormalizeWhitespace { get; set; } = true;

        [HtmlAttributeName("markdown")]
        public ModelExpression Markdown { get; set; }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            await base.ProcessAsync(context, output);

            string content = null;
            if (Markdown != null)
                content = Markdown.Model?.ToString();

            if (content == null)
                content = (await output.GetChildContentAsync(NullHtmlEncoder.Default))
                                .GetContent(NullHtmlEncoder.Default);

            if (string.IsNullOrEmpty(content))
                return;

            content = content.Trim('\n', '\r');

            output.TagName = null;
        }
    }
}
