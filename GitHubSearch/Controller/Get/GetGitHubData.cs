﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Text;
using GitHubSearch.Model;
using WebRequest = System.Net.WebRequest;
using ErrorLog = GitHubSearch.Controller.ErrorHandeling;

namespace GitHubSearch.Controller
{
    public class GetGitHubData : GitRepoData
    {

        private static HttpWebRequest GetWebRequest(string URL , string extension)
        {
            HttpWebRequest webRequest = WebRequest.Create(URL + extension) as HttpWebRequest;
            webRequest.Method = "GET";
            webRequest.UserAgent = "Anything";
            webRequest.ServicePoint.Expect100Continue = false;

            return webRequest;
        }
        private  static List<Item> GetRequest(string URL, string extension)
        {
            
            Root response = null;

            var webRequest = GetWebRequest(URL, extension);
                try
                {
                    using (StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
                    {
                        string reader = responseReader.ReadToEnd();

                        
                            response = JsonConvert.DeserializeObject<Root>(reader);
                        
                        
                    }
                }
                catch(Exception ex)
                {
                ErrorLog.LogError(ex);
                    return null;


                }
            return response.items.ToList();
        }
           
        

        private static Item GetSingleRequest(string URL, string extension)
        {
            //object jsonobj = null;
            GitRepoData.Item response = null;

            var webRequest = GetWebRequest(URL, extension);
                try
                {
                    using (StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
                    {
                        string reader = responseReader.ReadToEnd();


                        response = JsonConvert.DeserializeObject<Item>(reader);
                            return response;
                        
                    }
                }
                catch (Exception ex)
                {
                ErrorLog.LogError(ex);
                return null;


                }
        }
        

        private static ReadMe GetReadMeRequest(string URL, string extension)
        {
           
            ReadMe response = null;
            var webRequest = GetWebRequest(URL, extension);

                try
                {
                    using (StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
                    {
                        string reader = responseReader.ReadToEnd();


                        response = JsonConvert.DeserializeObject<ReadMe>(reader);
                        return response;

                    }
                }
                catch (Exception ex)
                {
                ErrorLog.LogError(ex);
                return null;

                }
            
        }
           
     

        public static Item GetFullNameResult(string username)
        {
            var URL = "https://api.github.com/repos/";
            var usernameExtension = username ;
            var jsonobj = GetSingleRequest(URL, usernameExtension);
            return jsonobj;

            
        }

       

        public static List<Item> SearchRepos( string name)
        {
            var URL = "https://api.github.com/search/repositories?q=";
            
            var searchCriteria = name;
            var  jsonobj =  GetRequest(URL, searchCriteria);
            return jsonobj;
            
        }

        public static string ReadMeResult(string username)
        {
            var URL = "https://api.github.com/repos/";
            string decodedString = "";
            var usernameExtension = username + "/contents/README.md";
            var jsonobj = GetReadMeRequest(URL, usernameExtension);
            if (jsonobj != null)
            {    
                 decodedString = DecodeString(jsonobj.Content);
            }
            return decodedString;

        }
        protected static string DecodeString(string encodedstring)
        {
            byte[] data = Convert.FromBase64String(encodedstring);
            return Encoding.UTF8.GetString(data);
        }

    }
}
