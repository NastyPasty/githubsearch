﻿using System;
using System.IO;

namespace GitHubSearch.Controller
{
	public class ErrorHandeling
	{
		public static void LogError(Exception ex)
		{
			string path = Directory.GetCurrentDirectory();
			string fileName = path + "errorLog.txt" ;

            try
            {
                // Check if file already exists. If yes, delete it.     
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }

                // Create a new file     
                using (StreamWriter writer = new StreamWriter("important.txt"))
                {

                    writer.Write(ex.Message, 0 );
                   
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }
	}
}