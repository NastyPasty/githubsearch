﻿using System;
using System.Web.UI;
using ErrorLog = GitHubSearch.Controller.ErrorHandeling;

namespace GitHubSearch
{
	public partial class _Default : Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			var error = Request.QueryString["Value"];

			if(error == "error")
			{
				SearchValueRepoNameErrorMessage.Text = "No Repository's found try something else.";
				SearchValueRepoNameErrorMessage.Visible = true;
			}
		}
		
		protected void btnSearchList_Click(object sender, EventArgs e)
		{

			
			var name = SearchValueRepoName.Text.ToString();
			if (name != "")
			{
				try
				{
					Response.Redirect("ListOfRepos.aspx?Value=" + name, false);
				}
				catch(Exception ex)
				{
					ErrorLog.LogError(ex);
				}
			}
			else
			{
				SearchValueRepoNameErrorMessage.Text = "Must enter name of Repository";
				SearchValueRepoNameErrorMessage.Visible = true;
			}
		}
		
	}
}