﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RepoDetails.aspx.cs" Inherits="GitHubSearch.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     <div class="info">
       <label>Repository Name: </label>
    <asp:Label runat="server"  ID ="repoName" />
       <br />
          <label>Homepage: </label>
    <asp:Label  runat="server"  ID ="repoHomePage" />
       <br />
    <label>Language: </label>
    <asp:Label runat="server" id="repoLanguage" />
       <br />
       <label>Open Issues: </label>
    <asp:Label runat="server" id="repoOpenIssues" />
       <br />
       <label>Forks: </label>
    <asp:Label runat="server" id="repoForks" />
       <br />      
       <label>Size: </label>
    <asp:Label runat="server" id="repoSize" />
       <br />
       <label>Watchers: </label>
    <asp:Label runat="server" id="repoWatchers" />
       <br />
       <label>Stargazers Count: </label>
    <asp:Label runat="server" id="repoStargazersCount" />
         <br/>
       <label>ReadMe: </label>
       <div id="markdown" runat="server"/>
   </div>
</asp:Content>
