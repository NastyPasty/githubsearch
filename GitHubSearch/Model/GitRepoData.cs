﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GitHubSearch.Model
{
	public class GitRepoData
	{


        public class Owner
        {
            public string login { get; set; }
            public int id { get; set; }

        }

        public class Item
        {

            public string name { get; set; }
           
            public string full_name { get; set; }
          
        public Owner owner { get; set; }
        public string html_url { get; set; }
        public string description { get; set; }
        public string homepage { get; set; }
        public int size { get; set; }
        public int stargazers_count { get; set; }
        public int watchers_count { get; set; }
        public string language { get; set; }
        public bool has_issues { get; set; }
        public bool has_projects { get; set; }
        public bool has_downloads { get; set; }
        public bool has_wiki { get; set; }
        public bool has_pages { get; set; }
        public int forks_count { get; set; }
        public object mirror_url { get; set; }
        public bool archived { get; set; }
        public bool disabled { get; set; }
        public int open_issues_count { get; set; }
        public object license { get; set; }
        public int forks { get; set; }
        public int open_issues { get; set; }
        public int watchers { get; set; }
        public string default_branch { get; set; }
        public double score { get; set; }
        public string type { get; set; }

        }

        public class ReadMe
        {
            public string Content { get; set; }
        }
    public class Root
    {
        public int total_count { get; set; }
        public bool incomplete_results { get; set; }
        public List<Item> items { get; set; }
       

        }


}
}