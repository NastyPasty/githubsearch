﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListOfRepos.aspx.cs" Inherits="GitHubSearch.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     <div class="info">
         <div>
            <asp:ListBox visible="true" Rows="10" id="ListResults" runat="server"/>
             <br />
             <asp:Label runat="server" Visible="false" ID="SearchValueRepoNameErrorMessage" /> 
             <br/>
        <asp:Button runat="server"    ID="btnListMoreDetails" OnClick="btnListMoreDetails_Click" Text="More Information" />
     </div>
   </div>
</asp:Content>
