﻿<%@ Page Async="true" Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="GitHubSearch._Default" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    

    <div class="info">
        <label>Search By Repository Name:</label>
        <br />
        <asp:TextBox name="SearchValueRepoName" id="SearchValueRepoName" runat="server" />
        <br />
        <asp:Label runat="server" Visible="false" ID="SearchValueRepoNameErrorMessage" /> 
        <br />
        <asp:Button runat="server" id="btnSearchList"  OnClick="btnSearchList_Click" Text="Search" ></asp:Button>
    </div>

  
   

</asp:Content>
