﻿using System;
using System.Web.UI;
using Markdig;
using GetGitData = GitHubSearch.Controller.GetGitHubData;


namespace GitHubSearch
{
	public partial class About : Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

			var RepositoryName = Request.QueryString["Value"];


			if (RepositoryName != "")
			{
				var repo = GetGitData.GetFullNameResult(RepositoryName);
				var readme = GetGitData.ReadMeResult(RepositoryName);

				repoName.Text = repo.full_name;
				repoOpenIssues.Text = repo.open_issues_count.ToString();
				repoForks.Text = repo.forks_count.ToString();
				repoHomePage.Text = repo.description;
				repoHomePage.Text = repo.homepage;
				repoSize.Text = repo.size.ToString();
				repoWatchers.Text = repo.watchers.ToString();
				repoLanguage.Text = repo.language;
				repoStargazersCount.Text = repo.stargazers_count.ToString();
				

				markdown.InnerHtml = Markdown.ToHtml(readme);
			}
		
		}
		
	}
}