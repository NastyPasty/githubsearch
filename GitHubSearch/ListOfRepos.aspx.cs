﻿using System;
using System.Linq;
using System.Web.UI;
using GitData = GitHubSearch.Model.GitRepoData.Item;
using GetGitData = GitHubSearch.Controller.GetGitHubData;
using System.Collections.Generic;
using ErrorLog = GitHubSearch.Controller.ErrorHandeling;

namespace GitHubSearch
{
	public partial class Contact : Page
	{
		protected void Page_Load (object sender, EventArgs e)
		{
			

			var response = new List<GitData>();
			

			var name = Request.QueryString["Value"];
			
				response = GetGitData.SearchRepos(name);
			try
			{
				if (response.Count != 0)
				{
					foreach (var item in response.Select(x => x.full_name))
					{
						ListResults.Items.Add(item);

					}
					ListResults.Visible = true;
				}
				else
				{
					Response.Redirect("Default.aspx?Value=" + "error", false);
				}
			}
			catch(Exception ex)
			{
				ErrorLog.LogError(ex);
			}
			
		}
		protected  void btnListMoreDetails_Click(object sender, EventArgs e)
		{
			var RepositoryName = ListResults.SelectedValue;
			if (RepositoryName != "")
			{
				try
				{
				
					Response.Redirect("RepoDetails.aspx?Value=" + RepositoryName, false);
				}
				catch(Exception ex)
				{
					ErrorLog.LogError(ex);
				}
			}
			else
			{
				SearchValueRepoNameErrorMessage.Text = "Must select a Repository";
				SearchValueRepoNameErrorMessage.Visible = true;
			}
		}
	}	
}