﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GetData = GitHubSearch.Controller.GetGitHubData;

namespace UnitTestProject
{
	[TestClass]
	public class GetUserApiDataUnitTest : GetData
	{
		[TestMethod]
		public void SpecificResults()
		{
			
			var repoData =  GetFullNameResult("facebook/react");
			Assert.IsNotNull(repoData);

		}
		

		[TestMethod]
		public void SearchRepos()
		{
			
			
			var repoData = SearchRepos("react");
			Assert.IsNotNull(repoData);
		}

		[TestMethod]
		public void ReadMeResult()
		{


			var repoData = ReadMeResult("facebook/react");
			Assert.IsNotNull(repoData);
		}
		[TestMethod]
		public void DecodeStringTest()
		{
			var encodedString = "SGVsbG8gV29ybGQ=";
			var decodedString = DecodeString(encodedString);

			Assert.AreEqual("Hello World",decodedString);
		}
	}
}
