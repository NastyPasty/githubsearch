﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GetRepo = GitHubSearch.Get.GetRepoOptions;
using Octokit;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace UnitTestProject
{
	[TestClass]
	public class RepoGetUnitTests
	{
		[TestMethod]
		public async Task RepoFullNameTest()
		{

			

		
			var repo = await GetRepo.GetRepo("facebook", "react");

			var fullName = GetRepo.GetRepoFullName(repo);

			Assert.AreEqual("facebook/react", fullName);

		}
		[TestMethod]
		public async Task RepoForks()
		{

			var repo = await GetRepo.GetRepo("facebook", "react");

			var forkCounts = GetRepo.GetRepoForks(repo);

			Assert.AreEqual(29830, forkCounts);

		}

		[TestMethod]
		public async Task RepoOpenIssues()
		{

			var repo = await GetRepo.GetRepo("facebook", "react");

			var openIssues = GetRepo.GetRepoOpenIssues(repo);

			Assert.AreEqual(589, openIssues);

		}
		[TestMethod]
		public async Task RepoReadMe()
		{

			var readme = await GetRepo.GetRepoReadMe("facebook", "react");

			Assert.IsNotNull(readme);

		}
	}
}
